<!DOCTYPE html>
<html>
<?php 
	include("security.inc");
	security("adminedit");
	?>
<head>
	<link rel="stylesheet" type="text/css" href="../../includes/styles.css">
</head>

<body>
<?php

include("../../live_connect/connect.inc");
if(isset($_POST['pw'])) {
	$un = strtolower($_POST['adminfname']) . '.' . strtolower($_POST['adminlname']);
	$pw = $_POST['pw'];
			
	if(isset($_POST['adminedit'])) {
		$adminedit = $_POST['adminedit'];
	}
	else { 
		$adminedit = ""; 
	}
    
    if(isset($_POST['home'])) {
		$home = $_POST['home'];
	}
	else { 
		$home = ""; 
	}

	$query = "INSERT INTO admin_glossary(username, password, adminedit, home) VALUES('".$un."', '".$pw."', '".$adminedit."', '".$home."')";
	mysqli_query($conn, $query);
	mysqli_close($conn);

}

if(isset($_POST['epassword'])) {
	
	if($_POST['eusername'] != '' && $_POST['epassword'] != '')
	{
		$eusername = strtolower($_POST['eusername']);
		$eoldusername = $_POST['eoldusername'];
		$epassword = $_POST['epassword'];
		
		if(isset($_POST['eadminedit'])) {
			$eadminedit = $_POST['eadminedit'];
		}
		else { 
			$eadminedit = ""; 
		}

		if(isset($_POST['ehome'])) {
			$ehome = $_POST['ehome'];
		}
		else { 
			$ehome = ""; 
		}
	}

		$query = mysqli_query($conn, "UPDATE admin_glossary SET username = '".$eusername."', password = '".$epassword."', 
		adminedit = '".$eadminedit."', home = '".$ehome."'
		WHERE username = '".$eoldusername."'");
		mysqli_close($conn);
		
		
}

if(isset($_POST['deleteusername'])) {
	if($_POST['deleteusername'] != '') {

		$deleteusername = $_POST['deleteusername'];
		mysqli_query($conn, "DELETE FROM admin_glossary WHERE username = '".$deleteusername."'");
		mysqli_close($conn);
	}
}
?>
<?php include("menu.inc");?>
<div class="wrapper">
<div class="inner-wrapper">


<div class="headingArea">
Admin Edit Page
</div>

<div class="widetextnoborder">
<form action="admin.php" method="POST">
<table>
	<tr><td class='tdh'>Fist Name:</td><td><input type="text" name="adminfname" required></td></tr>
	<tr><td class='tdh'>Last Name:</td><td><input type="text" name="adminlname" required></td></tr>
	<tr><td class='tdh'>Password:</td><td><input type="password" name="pw" required></td></tr>
    <tr><td class='tdh'>Home:</td><td><input type="checkbox" value='x' name="home" required></td></tr>
	<tr><td class='tdh'>Admin Edit:</td><td><input type='checkbox' name='adminedit' value='x'></td></tr>
	
	<tr><td class='tdb' colspan="2"><input type="submit" value="Add Admin"></td></tr>
</table>
</form>
</div>

<div class="headingArea">
Admin List
</div>

<?php
include("../../live_connect/connect.inc");
$query = mysqli_query($conn, "SELECT * FROM admin_glossary ORDER BY username ASC");
while($rows = mysqli_fetch_array($query)) {
	echo "<div class='adminedit'>
	<form action='admin.php' method='POST'>
	<table style='border-collapse: collapse;'>
		<tr><td class='tdh'>Username:</td><td><input type='text' name='eusername' value='" . $rows['username'] . "'>
			<input type='hidden' name='eoldusername' value='".$rows['username']."'></td></tr>
		<tr><td class='tdh'>Password:</td><td><input type='password' name='epassword' value='" . $rows['password'] . "'>
			<input type='hidden' name='eoldpassword' value='".$rows['password']."'></td></tr>
		
    <tr><td class='tdh'>Admin Edit:</td><td><input type='checkbox' name='eadminedit' value='x' "; if($rows['adminedit'] == 'x') { echo "checked='checked' >"; } echo "</td></tr>
    <tr><td class='tdh'>Home:</td><td><input type='checkbox' name='ehome' value='x' "; if($rows['home'] == 'x') { echo "checked='checked' >"; } echo "</td></tr>
	
	<tr><td class='tdb' colspan='2'><input style='float: right;' type='submit' value='Update'></form>
	<form action='admin.php' method='POST'>
	<input type='hidden' name='deleteusername' value='".$rows['username']."'>
	<input style='float: left;' type='submit' value='Delete'>
	</form></td></tr>
</table></div>";
}
mysqli_close($conn);
?>

</div>
</div>
</body>
</html>