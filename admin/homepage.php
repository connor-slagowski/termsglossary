<head>
	<link rel="stylesheet" type="text/css" href="../../includes/styles.css">
</head>

<?php
include("security.inc");
security("home");

    if(isset($_POST['word'])) {
        include("../../live_connect/connect.inc");

        $word = $_POST['word'];
        $desc = $_POST['desc'];
        $desc = htmlspecialchars($desc);
        $program = $_POST['program'];
        
        $date = date('Y-m-d h:i:s');
        

        $q = "INSERT INTO glossary(topic, def, program) VALUES ('".$word."', '".$desc."', '".$program."') ON DUPLICATE KEY UPDATE topic = '".$word."', def = '".$desc."', program = '".$program."'";
        mysqli_query($conn, $q) OR DIE("HAHA") ;

        mysqli_query($conn, "INSERT INTO glossary_tracking(topic, program, user, editdate) VALUES('".$word."', '".$program."', '".$_COOKIE['un']."', '".$date."') ON DUPLICATE KEY UPDATE
        topic = '".$word."', program = '".$program."', user = '".$_COOKIE['un']."', editdate = '".$date."'");
    }

    if(isset($_POST['topic'])) {
        include("../../live_connect/connect.inc");

        $topic = $_POST['topic'];
        $id = $_POST['id'];
        $utopic = $_POST['utopic'];
        $utopic = lcfirst($utopic);
        $prg = $_POST['prg'];
        $uprogram = $_POST['uprogram'];
        $def = $_POST['definition'];
        $def = htmlspecialchars($def);
        $date = date('Y-m-d h:i:s');

        $query = "UPDATE glossary SET topic = '".$topic."', def = '".$def."', program = '".$prg."' WHERE topic = '".$utopic."' AND program = '".$uprogram."'";
        mysqli_query($conn, $query) OR DIE("HAHA");

        mysqli_query($conn, "UPDATE glossary_tracking SET topic = '".$topic."', program = '".$prg."', user = '".$_COOKIE['un']."', editdate = '".$date."' WHERE topic = '".$utopic."' AND program = '".$uprogram."'");

    }

    if(isset($_POST['delete'])) {
        include("../../live_connect/connect.inc");
        $program = $_POST['dprogram'];
        $topic = $_POST['delete'];

        $query = "DELETE FROM glossary WHERE topic = '".$topic."' AND program = '".$program."'";
        mysqli_query($conn, $query) OR DIE("HAHA");

        mysqli_query($conn, "DELETE FROM glossary_tracking WHERE topic = '".$topic."' AND program = '".$program."'");
    }
?>

<?php include("menu.inc");?>
<div class="headingArea">
    Add a New Word to the Glossary
</div>
<table style="padding-left: 20px;">
<form method = "post" >
    <tr>
        <td>New word:</td>
        <td><input type="text" name="word"></td>
    </tr>
    <tr>
        <td alignment = "top">Definiton:</td>
        <td><textarea name="desc" rows="5" columns="100" style="resize:none"></textarea> </td>
    </tr>
    <tr>
        <td>Program:</td>
        <td><select name="program">
        <option></option>
        <?php
        include("../../live_connect/connect.inc");

            $query = mysqli_query($conn, "SELECT * FROM SchoolPrograms");

            while($rows = mysqli_fetch_array($query)) {
                $prog = $rows['program'];

                echo "<option value='".$prog."'>".$prog."</option>";
            }
        ?>
    </select></td> </tr>
    <tr>
        <td>
            <input type="submit" value="Submit">
        </td>
    </tr>
</form>
</table>

<div class="headingArea">
    Edit Existing Entries
</div>

<p>To narrow down the search you can select by:</p>
<form method='post'>
    First Letter:
    <select name ="alphabet">
        <option></option>
        <?php
        $alph = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        $alph = explode(",", $alph);
        foreach($alph as $alphabet) {
            echo "
                <option value='".$alphabet."'>".$alphabet."</option>
            ";
        }
        ?>
    </select>
     or Program:
    <select name="programs">
        <option></option>
        <?php
        $query = mysqli_query($conn, "SELECT * FROM SchoolPrograms ORDER BY program ASC");
        while($rows = mysqli_fetch_array($query)){
            $program=$rows['program'];
        echo"
        <option value='".$program."'>".$program."</option>
        ";
        }
        ?>
    </select>
   
    <input type='hidden' name='sort' value='sort'/>
    <input type='submit' name='submit' value = 'Sort'/>
</form>

<?php

if(isset($_POST['sort']) && isset($_POST['programs'])) {
    $query = mysqli_query($conn, "SELECT * FROM glossary WHERE program = '".$_POST['programs']."' ORDER BY topic ASC");

    echo "
        <table>";
            while($rows = mysqli_fetch_array($query)) {
                $word = $rows['topic'];
                $def = $rows['def'];
                $program = $rows['program'];
        echo"
            <form method ='post'>
                <tr>
                    <td>Word:</td>
                    <td style='background-color: lightblue;'>
                        <input type='hidden' name='utopic' value = '".$word."'/>
                        <input type='text' name='topic' value = '".ucwords($word)."' /></td>
                    <td >Program:</td>
                    <td style='background-color: lightblue;'>
                    <input type='hidden' name='uprogram' value='".$program."'/>
                        <select name='prg'>
                            <option></option>";
                            $query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms ORDER BY program ASC");
                            while($r = mysqli_fetch_array($query2)) {
                                $prog = $r['program'];
                                echo "<option value = '".$prog."' "; if($prog == $program) {echo "selected='selected'";} echo">".$prog."</option>";
                            }
                            echo"
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style='background-color: lightblue;'>Definition: </td>
                    <td>
                        <textarea style='resize:none' rows='3' cols='100'; name='definition' value=".$def.">".ucfirst($def)."</textarea>
                    </td>
                    <td align='center' >
                        <input type='submit' name='button' value='Update' style='margin-bottom: 7px; margin-top:10px;'/> <br />
                    </form>
                    <form method='post'>
                        <input type='hidden' name='dprogram' value='".$program."'/>
                        <input type='hidden' name='delete' value='".$word."'/>
                        <input type='submit' name='button' value='Delete' style='padding: 1px 8px'/>
                    </td>

                </tr>
                </form>
                <tr>
                    <td colspan = '4' align='center' style='background-color: lightblue;'>Last Update</td>";
                    $q = mysqli_query($conn, "SELECT * FROM glossary_tracking");
                    while($entry = mysqli_fetch_array($q)) {
                        if($entry['topic'] == $word && $entry['program'] == $program) {
                    
                echo "
                <tr>
                    <td>User: </td>
                    <td>".$entry['user']."</td>
                    <td>Last Updated:</td>
                    <td>".$entry['editdate']."</td>
                </tr>";
                        }
                }
                echo"                
                <tr colspan='4' style='height:15px; '>
                    <td style = 'background-color: lightgrey;' colspan= '4'></td>
                </tr>
            ";
        }
        echo"
        </table>
    ";
}

if(isset($_POST['sort']) && isset($_POST['alphabet'])) {
    $postletter = $_POST['alphabet'];
    $query = mysqli_query($conn, "SELECT * FROM glossary ORDER BY topic ASC");
    echo "
        <table>";
            while($rows = mysqli_fetch_array($query)) {
                $word = $rows['topic'];
                $letter = lcfirst(substr($word, 0, 1));
                $def = $rows['def'];
                $program = $rows['program'];
                if($letter == $postletter) {
        echo"
            <form method ='post'>
                <tr>
                    <td>Word:</td>
                    <td style='background-color: lightblue;'>
                        <input type='hidden' name='utopic' value = '".$word."'/>
                        <input type='text' name='topic' value = '".ucwords($word)."' /></td>
                    <td >Program:</td>
                    <td style='background-color: lightblue;'>
                    <input type='hidden' name='uprogram' value='".$program."'/>
                        <select name='prg'>
                            <option></option>";
                            $query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms ORDER BY program ASC");
                            while($r = mysqli_fetch_array($query2)) {
                                $prog = $r['program'];
                                echo "<option value = '".$prog."' "; if($prog == $program) {echo "selected='selected'";} echo">".$prog."</option>";
                            }
                            echo"
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style='background-color: lightblue;'>Definition: </td>
                    
                    <td>
                        <textarea style='resize:none' rows='3' cols='100'; name='definition' value=".$def.">".ucfirst($def)."</textarea>
                    </td>
                    <td align='center' >
                        <input type='submit' name='button' value='Update' style='margin-bottom: 7px; margin-top:10px;'/> <br />
                    </form>
                    <form method='post'>
                        <input type='hidden' name='dprogram' value='".$program."'/>
                        <input type='hidden' name='delete' value='".$word."'/>
                        <input type='submit' name='button' value='Delete' style='padding: 1px 8px'/>
                    </td>

                </tr>
                </form>
                <tr>
                    <td colspan = '4' align='center' style='background-color: lightblue;'>Last Update</td>";
                    $q = mysqli_query($conn, "SELECT * FROM glossary_tracking");
                    while($entry = mysqli_fetch_array($q)) {
                        if($entry['topic'] == $word && $entry['program'] == $program) {
                    
                echo "
                <tr>
                    <td>User: </td>
                    <td>".$entry['user']."</td>
                    <td>Last Updated:</td>
                    <td>".$entry['editdate']."</td>
                </tr>";
                        }
                }
                echo"                
                <tr colspan='4' style='height:15px; '>
                    <td style = 'background-color: lightgrey;' colspan= '4'></td>
                </tr>
            ";
        }
    }
        echo"
        </table>
    ";
} 
else {
    $query = mysqli_query($conn, "SELECT * FROM glossary ORDER BY topic ASC");

    echo "
        <table>";
            while($rows = mysqli_fetch_array($query)) {
                $word = $rows['topic'];
                $def = $rows['def'];
                $program = $rows['program'];
        echo"
            <form method ='post'>
                <tr>
                    <td>Word:</td>
                    <td style='background-color: lightblue;'>
                        <input type='hidden' name='utopic' value = '".$word."'/>
                        <input type='text' name='topic' value = '".ucwords($word)."' /></td>
                    <td >Program:</td>
                    <td style='background-color: lightblue;'>
                    <input type='hidden' name='uprogram' value='".$program."'/>
                        <select name='prg'>
                            <option></option>";
                            $query2 = mysqli_query($conn, "SELECT * FROM SchoolPrograms ORDER BY program ASC");
                            while($r = mysqli_fetch_array($query2)) {
                                $prog = $r['program'];
                                echo "<option value = '".$prog."' "; if($prog == $program) {echo "selected='selected'";} echo">".$prog."</option>";
                            }
                            echo"
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style='background-color: lightblue;'>Definition: </td>
                    <td>
                        <textarea style='resize:none' rows='3' cols='100'; name='definition' value=".$def.">".ucfirst($def)."</textarea>
                    </td>
                    <td align='center' >
                        <input type='submit' name='button' value='Update' style='margin-bottom: 7px; margin-top:10px;'/> <br />
                    </form>
                    <form method='post'>
                        <input type='hidden' name='dprogram' value='".$program."'/>
                        <input type='hidden' name='delete' value='".$word."'/>
                        <input type='submit' name='button' value='Delete' style='padding: 1px 8px'/>
                    </td>

                </tr>
                </form>
                <tr>
                    <td colspan = '4' align='center' style='background-color: lightblue;'>Last Update</td>";
                    $q = mysqli_query($conn, "SELECT * FROM glossary_tracking");
                    while($entry = mysqli_fetch_array($q)) {
                        if($entry['topic'] == $word && $entry['program'] == $program) {
                    
                echo "
                <tr>
                    <td>User: </td>
                    <td>".$entry['user']."</td>
                    <td>Last Updated:</td>
                    <td>".$entry['editdate']."</td>
                </tr>";
                        }
                }
                echo"                
                <tr colspan='4' style='height:15px; '>
                    <td style = 'background-color: lightgrey;' colspan= '4'></td>
                </tr>
            ";
        }
        echo"
        </table>
    ";
} 
?>