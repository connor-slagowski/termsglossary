<?
    session_start();
?>

<style>

.tgl-btn {
    width: 50px;
    margin: 10px;
    padding: 5px;
    border-radius: 0px 15px;
    background-color: lightgrey;
}
.btn:checked ~ .tgl-btn {
    background-color: #4f4f4f;
    color:white;
}

.tgl-btn:hover {
    background-color:grey;
    color: white;
}
ul li {
    display:inline;
    list-style-type: none;
}


</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<?php
include("../live_connect/connect.inc");

if(isset($_POST['program']) && $_POST['program'] != '') { 
$query = mysqli_query($conn, "SELECT * FROM glossary WHERE program = '".$_POST['program']."' ORDER BY topic ASC");

    while($rows = mysqli_fetch_array($query)) {
        $topic = $rows['topic'];
        $firstletter = substr($topic, 0, 1);
        $alphabet[] = $firstletter;
        $alphabet = array_unique($alphabet);
    }
}
else {
    $query = mysqli_query($conn, "SELECT * FROM glossary ORDER BY topic ASC");
    
    while($rows = mysqli_fetch_array($query)) {
        $topic = $rows['topic'];
        $firstletter = substr($topic, 0, 1);
        $alphabet[] = $firstletter;
        $alphabet = array_unique($alphabet);
    }
}
?>

<script>
    function getValue() {
        var p = 0;
        var arr = new Array();
        <?php foreach($alphabet as $alph) { ?>
            var chkbx = document.getElementById('btn<?php echo $alph;?>');

            if(chkbx.checked == true && arr.indexOf('<?php echo $alph?>') == -1) {
                arr[p] = chkbx.value;
                p++;
            }
           
        <?php } ?>
            
            document.getElementById('sub').value = arr;

            if(document.getElementById('sub').value != '') {
                document.getElementById('myForm').submit();
            }

            var buttonChecked = document.querySelectorAll('input[type=checkbox]:checked');
            if(buttonChecked.length == 0) {
                document.getElementById('table').style.display = "none";
            }
    }

    <?php if(isset($_GET['letter'])) { 
     foreach($alphabet as $alph) { ?>
        var chkbx = document.getElementById('btn<?php echo $alph; ?>');

        if(chkbx.value = <?php echo $_GET['letter']; ?>) {
            chkbx.checked = 'true';
            getValue();
            unset($_GET['letter']);
        }
     <?php } 
     }?>
</script>

<form method='post' action='glossary.php' id='myForm' name='myForm'>
    <ul class = "btns">
    <?php 
    foreach($alphabet as $al) { 
       echo '<li>
            <input type="checkbox" id="btn'.$al.'" class="btn"  hidden="hidden"  value="'.$al.'" OnClick="getValue()"'; 
                if(strlen($_POST['sub']) > 1) {
                    $sub = explode(',', $_POST['sub']);
                    for($i=0; $i < count($sub); $i++) {
                        if($al == $sub[$i]) {
                            echo 'checked = "checked"';
                        }
                    }
                }
                elseif($al == $_POST['sub']) {
                    echo 'checked="checked"';
                } 
                elseif($al == $GET['letter']) {
                    echo 'checked="checked"';
                } 
            echo '/>
                <label class="tgl-btn" for="btn'.$al.'">'.strtoupper($al).'</label>
        </li>';
    }
    ?>
    </ul>
    <input type='hidden' name='program' value='<?php if(isset($_POST['program'])) {echo $_POST['program'];} ?>'/>
    <input type="text" name='sub' id='sub' value='' hidden='hidden'/>
</form>


<p id='text'>
<?php
if(isset($_POST['program']) && $_POST['program'] != '') {

$query = mysqli_query($conn, "SELECT * FROM glossary WHERE program = '".$_POST['program']."' ORDER BY topic ASC");


    echo "<table id='table'>";
while($r = mysqli_fetch_array($query)) {
    
    $topic = $r['topic'];
    $description = $r['def'];
    $firstletter = substr($topic, 0, 1);
   
    if(strlen($_POST['sub']) > 1) {
        $sub = explode(',', $_POST['sub']);

        for($i=0; $i < count($sub); $i++) {
            if($firstletter == $sub[$i]) {
                echo "
                <tr class='row".$firstletter."' >
                    <td style = 'font-weight:bold'>".ucwords($topic).":</td>
                    <td>".ucfirst($description)."</td>
                </tr>";
            }
        }
    }
    elseif($firstletter == $_POST['sub']) {

    echo "
        <tr class='row".$firstletter."' >
            <td style = 'font-weight:bold'>".ucwords($topic).":</td>
            <td>".ucfirst($description)."</td>
        </tr>";
        
    }
    
}
echo "</table>";
}

else {
    $query = mysqli_query($conn, "SELECT DISTINCT topic, def FROM glossary ORDER BY topic ASC");


    echo "<table id='table'>";
while($r = mysqli_fetch_array($query)) {
    
    $topic = $r['topic'];
    $description = $r['def'];
    $firstletter = substr($topic, 0, 1);
   
    if(strlen($_POST['sub']) > 1) {
        $sub = explode(',', $_POST['sub']);

        for($i=0; $i < count($sub); $i++) {
            if($firstletter == $sub[$i]) {
                echo "
                <tr class='row".$firstletter."' >
                    <td style = 'font-weight:bold'>".ucwords($topic).":</td>
                    <td>".ucfirst($description)."</td>
                </tr>";
            }
        }
    }
    elseif($firstletter == $_POST['sub']) {

    echo "
        <tr class='row".$firstletter."' >
            <td style = 'font-weight:bold'>".ucwords($topic).":</td>
            <td>".ucfirst($description)."</td>
        </tr>";
        
    }
    elseif($firstletter == $_GET['letter']) {

        echo "
            <tr class='row".$firstletter."' >
                <td style = 'font-weight:bold'>".ucwords($topic).":</td>
                <td>".ucfirst($description)."</td>
            </tr>";
            
        }
    
}
echo "</table>";
}

?>



</p>

